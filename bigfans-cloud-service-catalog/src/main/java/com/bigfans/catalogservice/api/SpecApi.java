package com.bigfans.catalogservice.api;

import java.util.List;

import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.catalogservice.service.spec.SpecOptionService;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpecApi {
	
	@Autowired
	private SpecOptionService optionService;

	@GetMapping(value = "/specs")
	public RestResponse list(
			@RequestParam(value = "catId" , required = false) String catId,
			@RequestParam(value = "prodId" , required = false) String prodId
	) throws Exception{
		assert (catId != null || prodId != null);
		List<SpecOption> specOptions = null;
		if(StringHelper.isNotEmpty(catId)){
			specOptions = optionService.listByCatId(catId, null, null);
		}
		if(StringHelper.isNotEmpty(prodId)){
			specOptions = optionService.listByPid(prodId, null, null);
		}
		return RestResponse.ok(specOptions);
	}
	
	@PostMapping(value = "/spec")
	public RestResponse create(@RequestBody SpecOption specOption) throws Exception{
		optionService.create(specOption);
		return RestResponse.ok();
	}
	
}
