package com.bigfans.catalogservice.model;

import com.bigfans.Constants;
import com.bigfans.catalogservice.model.entity.ProductEntity;
import com.bigfans.framework.utils.ArithUtils;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Description:商品
 * @author lichong 2015年3月18日上午10:48:41
 *
 */
@Data
public class Product extends ProductEntity {

	private static final long serialVersionUID = -4961274117193266934L;

	// 商品类别名称
	private String categoryName;
	// 商品品牌
	private String brandName;
	// 商品库存数量
	protected Integer stock;
	// 优惠后的价格
	private BigDecimal discountedPrice;
	// 属性
	private List<AttributeValue> attrValueList = new ArrayList<AttributeValue>();
	// 标签
	private List<Tag> tagList = new ArrayList<Tag>();
	// 表单项: 图片列表
	private List<ProductImage> imgList;
	// 表单项: 规格列表
	private List<ProductSpec> specList;

	// 节省金额
	public BigDecimal getSavedMoney() {
		if (discountedPrice == null) {
			return Constants.ZERO;
		}
		return ArithUtils.sub(price, discountedPrice);
	}

	@Override
	public String toString() {
		return "Product [origin=" + origin + ", categoryId=" + categoryId + ", categoryName=" + categoryName
				+ ", brandId=" + brandId + ", brandName=" + brandName + ", pgId=" + pgId + ", sn=" + sn + ", name="
				+ name + ", price=" + price + ", imagePath=" + imagePath + ", id=" + id + "]";
	}

}
