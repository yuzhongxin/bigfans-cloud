package com.bigfans.catalogservice.service.spec;

import com.bigfans.catalogservice.dao.SpecValueDAO;
import com.bigfans.catalogservice.model.SpecValue;
import com.bigfans.framework.dao.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月30日下午10:53:15
 *
 */
@Service(SpecValueServiceImpl.BEAN_NAME)
public class SpecValueServiceImpl extends BaseServiceImpl<SpecValue> implements SpecValueService {

	public static final String BEAN_NAME = "specValueService";
	
	private SpecValueDAO specValueDAO;
	
	@Autowired
	public SpecValueServiceImpl(SpecValueDAO specValueDAO) {
		super(specValueDAO);
		this.specValueDAO = specValueDAO;
	}

	@Override
	public List<SpecValue> listById(String[] ids) throws Exception{
		return specValueDAO.listById(ids);
	}
	
	@Override
	public List<SpecValue> listByOptionId(String optionId) throws Exception {
		return specValueDAO.listByOptionId(optionId);
	}
	
}
