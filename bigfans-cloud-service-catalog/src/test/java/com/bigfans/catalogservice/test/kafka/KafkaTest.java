package com.bigfans.catalogservice.test.kafka;

import com.bigfans.catalogservice.CatalogServiceApp;
import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.ProductCreatedEvent;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Properties;

/**
 * @author lichong
 * @create 2018-02-11 下午8:31
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=CatalogServiceApp.class)
public class KafkaTest {

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Test
    public void test(){
        kafkaTemplate.send(new ProductCreatedEvent("1"));
    }

}
