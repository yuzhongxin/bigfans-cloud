package com.bigfans.orderservice.api;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.OrderItem;
import com.bigfans.orderservice.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderItemApi extends BaseController {

    @Autowired
    private OrderItemService orderItemService;

    @GetMapping(value = "/unCommentedItems")
    @NeedLogin
    public RestResponse getUnCommentedItems(@RequestParam(value = "orderId") String orderId) throws Exception {
        CurrentUser cu = Applications.getCurrentUser();
        List<OrderItem> unCommentedItems = orderItemService.listUnCommentedItems(cu.getUid(), orderId);
        return RestResponse.ok(unCommentedItems);
    }

}
