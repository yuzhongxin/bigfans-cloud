package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.orderservice.dao.DeliveryMethodDAO;
import com.bigfans.orderservice.model.DeliveryMethod;
import org.springframework.stereotype.Repository;


/**
 * 
 * @Description:配送类型DAO操作
 * @author lichong
 * 2015年4月6日上午9:58:12
 *
 */
@Repository(DeliveryMethodDAOImpl.BEAN_NAME)
public class DeliveryMethodDAOImpl extends MybatisDAOImpl<DeliveryMethod> implements DeliveryMethodDAO {

	public static final String BEAN_NAME = "deliveryMethodDAO";
}
