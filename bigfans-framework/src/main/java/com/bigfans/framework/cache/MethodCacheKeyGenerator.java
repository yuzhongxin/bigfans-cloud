package com.bigfans.framework.cache;

import java.lang.reflect.Method;

/**
 * 
 * @Title: 
 * @Description: 按照拼接方法名和参数的方式生成key值
 * @author lichong 
 * @date 2016年1月24日 下午7:55:26 
 * @version V1.0
 */
public class MethodCacheKeyGenerator implements CacheKeyGenerator {

	@Override
	public String generate(Object className, Method method, Object... params) {
		StringBuilder keyStr = new StringBuilder();
		keyStr.append(className);
		keyStr.append(".");
		keyStr.append(method.getName());
		if (params.length != 0) {
			for (Object p : params) {
				keyStr.append("-");
				keyStr.append(p);
			}
		}
		return keyStr.toString();
	}

}
