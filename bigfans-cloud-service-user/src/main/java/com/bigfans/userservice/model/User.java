package com.bigfans.userservice.model;

import com.bigfans.userservice.model.entity.UserEntity;
import lombok.Data;

@Data
public class User extends UserEntity {

	private static final long serialVersionUID = -4074625277326161557L;
	
	/** 注册表单数据 */
	private String verificationCode;
	private String confirmedPassword;
	private Boolean agreeTerms;
	private String returnUrl;

	/* 登录表单数据 */
	private boolean rememberMe;
	
}
