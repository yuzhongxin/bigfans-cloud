package com.bigfans.userservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.userservice.dao.UserDAO;
import com.bigfans.userservice.model.User;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository(UserDAOImpl.BEAN_NAME)
public class UserDAOImpl extends MybatisDAOImpl<User> implements UserDAO {

	public static final String BEAN_NAME = "userDAO";
	
	public int countByAccount(String account){
		ParameterMap params = new ParameterMap();
		params.put("account", account);
		return getSqlSession().update(className + ".count", params);
	}

	@Override
	public int usePoints(String userId, Float points) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("points", points);
		return getSqlSession().update(className + ".useBenefits", params);
	}

	@Override
	public int useBalance(String userId, BigDecimal balance) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("balance", balance);
		return getSqlSession().update(className + ".useBenefits", params);
	}
	
}