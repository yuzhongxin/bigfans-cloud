package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.User;

import java.math.BigDecimal;

public interface UserDAO extends BaseDAO<User> {
	
	int usePoints(String userId, Float points);
	
	int useBalance(String userId, BigDecimal balance);
	
	int countByAccount(String account);
	
}