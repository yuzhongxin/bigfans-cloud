package com.bigfans.model.dto.order;

import com.bigfans.framework.utils.ArithUtils;
import com.bigfans.model.dto.cart.CartItemPromotionDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lichong
 * @create 2018-02-17 下午12:52
 **/
@Data
public class OrderPricingResultDto {

    private BigDecimal originalTotalPrice;
    private BigDecimal totalPrice;
    private Integer prodTotalQuantity;
    private BigDecimal freight;
    private BigDecimal couponDeductionTotal;
    private BigDecimal pointDeductionTotal;
    private BigDecimal balanceDeductionTotal;
    private Map<String , List<OrderItemPromotionDto>> promotionMap = new HashMap<>();
    private Map<String , OrderItemPricingResultDto> priceMap = new HashMap<>();

    public void addItemResult(OrderItemPricingResultDto itemCalculateResultDto){
        this.priceMap.put(itemCalculateResultDto.getProdId() , itemCalculateResultDto);
    }

    public List<OrderItemPromotionDto> getPromotion(String prodId) {
        return promotionMap.get(prodId);
    }

    public BigDecimal getSavedMoney(){
        BigDecimal savedMoney = BigDecimal.ZERO;
        for(Map.Entry<String , OrderItemPricingResultDto> itemResult : priceMap.entrySet()){
            savedMoney = ArithUtils.add(savedMoney , itemResult.getValue().getSavedMoney());
        }
        return savedMoney;
    }

    public void addPromotion(String prodId , OrderItemPromotionDto promotionDto){
        List<OrderItemPromotionDto> promotionDtoList = promotionMap.get(prodId);
        if(promotionDtoList == null){
            promotionDtoList = new ArrayList<>();
        }
        promotionDtoList.add(promotionDto);
        promotionMap.put(prodId , promotionDtoList);
    }

    public OrderItemPricingResultDto getPrice(String prodId){
        return priceMap.get(prodId);
    }
}
