package com.bigfans.model.event.attribute;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

import java.util.List;

/**
 * @author lichong
 * @create 2018-02-15 上午8:58
 **/
@Data
public class AttributeCreatedEvent extends AbstractEvent {

    protected String prodId;
    protected String pgId;
    protected String categoryId;
    protected String name;
    protected String code;
    protected Integer orderNum;
    protected String inputType;
    protected Boolean searchable;
    protected Boolean required;
    protected List<String> values;
}
