package com.bigfans.model.event.payment;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderPaidEvent extends AbstractEvent{

    /* order id*/
    private String orderId;

    public OrderPaidEvent(String orderId) {
        this.orderId = orderId;
    }
}
