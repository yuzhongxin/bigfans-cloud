package com.bigfans.model.event.user;

import com.bigfans.framework.event.AbstractEvent;

public class AddressCreatedEvent extends AbstractEvent {

    private String id;

    public AddressCreatedEvent(String id) {
        this.id = id;
    }
}
