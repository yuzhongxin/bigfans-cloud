package com.bigfans.cartservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;

/**
 * 
 * @Title: 
 * @Description: 限购异常
 * @author lichong 
 * @date 2016年3月6日 下午1:15:13 
 * @version V1.0
 */
public class ExceedLimitBuyException extends ServiceRuntimeException {

	private static final long serialVersionUID = -7536159302473716068L;

	public ExceedLimitBuyException() {
	}
	
	public ExceedLimitBuyException(Throwable e) {
		super(e);
	}
	
	public ExceedLimitBuyException(String message) {
		super(message);
	}
}
