package com.bigfans.searchservice.document.convertor;

import com.bigfans.framework.es.DocumentConverter;
import com.bigfans.framework.es.IndexDocument;
import com.bigfans.framework.utils.ArithUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.searchservice.model.Product;
import com.bigfans.searchservice.schema.mapping.ProductMapping;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月8日上午9:40:58
 *
 */
public class ProductDocumentConverter implements DocumentConverter<Product> {

	@Override
	public Product toObject(Map<String, Object> dataMap) {
		Product product = new Product();
		
		String id = StringHelper.toString(dataMap.get(ProductMapping.FIELD_ID));
		String name = StringHelper.toString(dataMap.get(ProductMapping.FIELD_NAME));
		String imagePath = StringHelper.toString(dataMap.get(ProductMapping.FIELD_IMAGE_PATH));
		BigDecimal price = ArithUtils.toBigDecimal(dataMap.get(ProductMapping.FIELD_PRICE));
		Object attrObj = dataMap.get(ProductMapping.FIELD_ATTRIBUTES);
//		if(attrObj != null){
//			List<AttributeValue> attrValueList = (List<AttributeValue>) dataMap.get(ProductMapping.FIELD_ATTRIBUTES);
//			product.setAttrValueList(attrValueList);
//		}
		
		product.setId(id);
		product.setName(name);
		product.setImagePath(imagePath);
		product.setPrice(price);
		return product;
	}

	@Override
	public IndexDocument toDocument(Product obj) {
		IndexDocument doc = new IndexDocument(obj.getId());
		doc.put(ProductMapping.FIELD_ID, obj.getId());
		doc.put(ProductMapping.FIELD_ORIGIN, obj.getOrigin());
		doc.put(ProductMapping.FIELD_BRAND, obj.getBrandName());
		doc.put(ProductMapping.FIELD_BRAND_ID, obj.getBrandId());
		doc.put(ProductMapping.FIELD_NAME, obj.getName());
		doc.put(ProductMapping.FIELD_IMAGE_PATH, obj.getImagePath());
		doc.put(ProductMapping.FIELD_PRICE, obj.getPrice());
		doc.put(ProductMapping.FIELD_CATEGORY_ID, obj.getCategoryId());
		doc.put(ProductMapping.FIELD_CATEGORY, obj.getCategoryName());
//		// 规格
//		List<ProductSpec> specList = obj.getSpecList();
//		if(CollectionUtils.isNotEmpty(specList)){
//			List<Map<String,Object>> specData = new ArrayList<Map<String,Object>>();
//			for (ProductSpec ps : specList) {
//				Map<String,Object> specMap = new HashMap<String,Object>();
//				specMap.put(ProductMapping.FIELD_SPEC_OPTID, ps.getOptionId());
//				specMap.put(ProductMapping.FIELD_SPEC_OPTNAME, ps.getOptionName());
//				specMap.put(ProductMapping.FIELD_SPEC_VALID, ps.getValueId());
//				specMap.put(ProductMapping.FIELD_SPEC_VALNAME, ps.getValue());
//				specData.add(specMap);
//			}
//			doc.put(ProductMapping.FIELD_SPECS, specData);
//		}
//		//  属性字段
//		List<AttributeValue> attrValueList = obj.getAttrValueList();
//		if(CollectionUtils.isNotEmpty(attrValueList)){
//			List<Map<String,Object>> attrData = new ArrayList<Map<String,Object>>();
//			for (AttributeValue avv : attrValueList) {
//				Map<String,Object> attrMap = new HashMap<String,Object>();
//				attrMap.put(ProductMapping.FIELD_ATTRIBUTE_OPTID, avv.getOptionId());
//				attrMap.put(ProductMapping.FIELD_ATTRIBUTE_OPTNAME, avv.getOptionName());
//				attrMap.put(ProductMapping.FIELD_ATTRIBUTE_VALID, avv.getId());
//				attrMap.put(ProductMapping.FIELD_ATTRIBUTE_VALNAME, avv.getValue());
//				attrData.add(attrMap);
//			}
//			doc.put(ProductMapping.FIELD_ATTRIBUTES, attrData);
//		}
//		// 标签
//		List<Tag> tags = obj.getTagList();
//		List<Map<String , Object>> tagDatas = new ArrayList<Map<String , Object>>();
//		for(Tag t : tags){
//			Map<String , Object> tagData = new HashMap<String , Object>();
//			tagData.put(ProductMapping.FIELD_TAG_ID, t.getId());
//			tagData.put(ProductMapping.FIELD_TAG_NAME, t.getName());
//			tagDatas.add(tagData);
//		}
//		doc.put(ProductMapping.FIELD_TAGS, tagDatas);
//
//		// 商品特征,用于商品推荐
//		StringBuilder features = new StringBuilder(30);
//		features.append(ProductMapping.FIELD_CATEGORY).append("=").append(obj.getCategoryName()).append(Constant.SPACE);
//		features.append(ProductMapping.FIELD_BRAND).append("=").append(obj.getBrandName()).append(Constant.SPACE);
//		if(CollectionUtils.isNotEmpty(attrValueList)){
//			for (AttributeValue av : attrValueList) {
//				features.append(av.getOptionId()).append("=").append(av.getValue()).append(Constant.SPACE);
//			}
//		}
//		doc.put(ProductMapping.FIELD_FEATURES, features.toString());
		return doc;
	}

}
