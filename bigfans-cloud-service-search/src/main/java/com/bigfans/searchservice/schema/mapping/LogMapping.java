package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class LogMapping extends BaseMapping {

	public static final String TYPE = "Log";
	public static final String INDEX = "log";
	public static final String ALIAS = "log_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_CREATE_DATE = "ceateDate";
	public static final String FIELD_UPDATE_DATE = "updateDate";
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	@Override
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
								.endObject()
								.startObject(FIELD_CREATE_DATE)  
						            .field("type", "date")  
						        .endObject()
						        .startObject(FIELD_UPDATE_DATE)  
						            .field("type", "date")  
						        .endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
