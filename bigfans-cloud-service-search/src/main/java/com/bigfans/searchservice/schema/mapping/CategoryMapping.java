package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import com.bigfans.framework.es.schema.IndexSettingsAnalyzer_Pinyin;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class CategoryMapping extends BaseMapping {

	public static final String INDEX = "category";
	public static final String TYPE = "category";
	public static final String ALIAS = "category_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_NAME_RAW = "raw";
	public static final String FIELD_NAME_PINYIN = "pinyin";
	public static final String FIELD_PARENT_ID = "parent_id";
	public static final String FIELD_LEVEL = "level";

	@Override
	public String getType() {
		return TYPE;
	}
	
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
									.field("store", true)
								.endObject()
								.startObject(FIELD_NAME)
									.field("type", "text")
									.field("index", true)
								    .field("analyzer", IndexSettingsAnalyzer_Pinyin.NAME)
									.startObject("fields")
					            		.startObject(FIELD_NAME_RAW)
					            			.field("type", "keyword")
					            		.endObject()
						            .endObject()
								.endObject()
						        .startObject(FIELD_PARENT_ID)
						            .field("type", "keyword")
						        .endObject()
								.startObject(FIELD_LEVEL)
									.field("type", "integer")
								.endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
